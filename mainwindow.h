#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "score_input.h"
#include "course.h"

#include <QMainWindow>
#include <QGridLayout>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QPushButton>
#include <QComboBox>
#include <QRadioButton>
#include <QVector>


QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    //Accessor
    enum course_name get_current_display() ;

private slots:
    void change_class(QString);

private:
    Ui::MainWindow *ui;
    QWidget * centralW;

    QGridLayout * main_layout;

    QHBoxLayout * top;    //Course Selection
    QHBoxLayout * bottom; //Grading Schema and Grade Display

    //Course Selection
    QLabel * head;
    QComboBox * course_select;

    //Grading Schema
    QRadioButton * method_A;
    QRadioButton * method_B;

    //Grade Display
    QLabel * btm_label;

    //Which class is in focus/on the window?
    enum course_name current_display;

    class Course * pic10b;
    class Course * pic10c;

};
#endif // MAINWINDOW_H
