#ifndef COURSE_H
#define COURSE_H

#include "score_input.h"
#include "mainwindow.h"

#include <QObject>
#include <QVector>
#include <QVBoxLayout>
#include <QPushButton>

//enum for Grading Breakdown
enum Grading_Breakdown { A, B};

//enum for course name
enum course_name { PIC10B, PIC10C,};

class Course : public QObject
{
    Q_OBJECT
public:
    Course(course_name c = PIC10B, QObject *parent = nullptr);

    //Accessors for Columns
    QVBoxLayout * get_left() const;
    QVBoxLayout * get_right() const;

    //Accessor for Calculate_Button
    QPushButton * get_calculate_button() const;

    //Accessor for Grade Label
    QLabel * get_grade() const;

    //Display or Hide Contents of Course
    void display_on();
    void display_off();

private slots:
    void change_grading();
    void calculate_grade();

private:
    course_name name;

    QVector<score_input*> grades;

    QVBoxLayout * left_column;//for HW input
    QVBoxLayout * right_column;//for exam input

    Grading_Breakdown method;

    QPushButton * calculate;
    QLabel * grade;

};

#endif // COURSE_H
