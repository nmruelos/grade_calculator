#include "course.h"

/* ~~~~~~~~~~ Constructor ~~~~~~~~~~ */
Course::Course(enum course_name c, QObject *parent) :  QObject(parent), name(c)
{
    method = A;

    left_column= new QVBoxLayout;
    right_column= new QVBoxLayout;

    int hwm_num;
    int mid_num;

    if (name == PIC10B){
        hwm_num = 8;
        mid_num = 2;
    }else{ //PIC10C
        hwm_num = 3;
        mid_num = 1;
    }

    //Homeworks
    for (int i = 0; i < hwm_num; i++){
        QString hwm_name = "HW " + QString::number(i+1);

        score_input * hw = new score_input(hwm_name, HW);
        grades.push_back(hw);
        left_column->addWidget(grades[i]->get_group());
    }

    //Midterms
    for (int i = 0; i < mid_num; i++){
        QString mid_name = "Midterm " + QString::number(i+1);

        score_input * mid = new score_input(mid_name, MIDTERM);
        grades.push_back(mid);
        right_column->addWidget(grades[i+hwm_num]->get_group());
    }

    //Final Exam
    score_input * final = new score_input("Final", FINAL);
    grades.push_back(final);
    right_column->addWidget(final->get_group());

    //Final Project - PIC10C Only
    if (name == PIC10C){
        score_input * project = new score_input("Project", PROJECT);
        grades.push_back(project);
        right_column->addWidget(project->get_group());
    }

    calculate = new QPushButton("Calculate Grade");
    calculate->setFixedSize(175,30);
    QObject::connect( calculate, SIGNAL ( clicked() ),
                     this, SLOT ( calculate_grade() ) );
    grade = new QLabel(" ");
}

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
/* ~~~~~~~~~~~~~~~~~~ Accessors ~~~~~~~~~~~~~~~~~~ */
/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

QVBoxLayout *Course::get_left() const
{
    return left_column;
}

QVBoxLayout *Course::get_right() const
{
    return right_column;
}

QPushButton *Course::get_calculate_button() const
{
    return calculate;
}

QLabel *Course::get_grade() const
{
    return grade;
}

/* ~~~~~~~~ Turn On/Off Class on Main Window ~~~~~~~~ */
void Course::display_on()
{
    for (int i=0; i < grades.size(); i++){
        grades[i]->widgets_on();
    }
    calculate->show();
    grade->show();
}

void Course::display_off()
{
    for (int i=0; i < grades.size(); i++){
        grades[i]->widgets_off();
    }
    calculate->hide();
    grade->hide();
}

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
/* ~~~~~~~~~~~~~~~~ Private Slots ~~~~~~~~~~~~~~~~ */
/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

/* ~~~~~~~~~~ Changes Grading Method ~~~~~~~~~~ */
void Course::change_grading()
{
    if (method == A){
        method = B;

    }else{
        method = A;

    }
}

/* ~~~~~~~~~~ Calculate the Final Grade ~~~~~~~~~~ */
//Warning...very messy :(
/* Requires:
 * vector of scores,
 * weight of each score (ENUM type),
 * Class (PIC 10B or C)
 * Grading Scheme (Method A or B) */
void Course::calculate_grade(){
    double final_grade = 0;

    if (name == PIC10B){
        if (method == A){
            double lowest_hw = 0;
            for (int i=0; i < grades.size(); i++){
                if (grades[i]->get_type() == HW) {
                    //finding lowest hw
                    if (grades[i]->get_score() < lowest_hw){
                        lowest_hw = grades[i]->get_score();
                    }
                    //the HW category is worth 25% in total, and has 8
                    // 25/8
                    final_grade += (grades[i]->get_score() * (.25/8));
                }
                else if (grades[i]->get_type() == MIDTERM){
                    //each midterm is worth 20%
                    final_grade += (grades[i]->get_score() * .2);
                }
                else{ //type == FINAL
                    //the final is worth 35%
                    final_grade += grades[i]->get_score() * .35;
                }
                final_grade -= (lowest_hw * (.25/8));
            }
        }
        else{//method B
            double highest_midterm = 0;
            for (int i=0; i < grades.size(); i++){
                if (grades[i]->get_type() == HW) {
                    //the HW category is worth 25% in total, and has 8
                    // 25/8
                    final_grade += (grades[i]->get_score() * (.25/8));
                }
                else if (grades[i]->get_type() == MIDTERM){
                    //Highest Midterm is worth 30%
                    if (grades[i]->get_score() > highest_midterm){
                        highest_midterm = grades[i]->get_score();
                    }
                }
                else{// type == FINAL
                    //the final is worth 35%
                    final_grade += (grades[i]->get_score() * .44);
                }

                final_grade += (highest_midterm * .30);
            }
        }
    }
    else{
        if (method == A){
            for (int i=0; i < grades.size(); i++){
                if (grades[i]->get_type() == HW) {
                    //the HW category is worth 15% in total, and has 3
                    final_grade += (grades[i]->get_score() * (.15/3));
                }
                else if (grades[i]->get_type() == MIDTERM){
                    //each midterm is worth 25%
                    final_grade += (grades[i]->get_score() * .25);
                }
                else if (grades[i]->get_type() == FINAL){
                    //the final is worth 30%
                    final_grade += grades[i]->get_score() * .3;
                }
                else {
                    //Final Project is 35%
                    final_grade += grades[i]->get_score() * .35;
                }

            }
        }
        else{//method B
            for (int i=0; i < grades.size(); i++){
                if (grades[i]->get_type() == HW) {
                    //the HW category is worth 25% in total, and has 3
                    final_grade += (grades[i]->get_score() * (.15/3));
                }
                else if (grades[i]->get_type() == FINAL){
                    //FINAL is worth .5
                    final_grade += (grades[i]->get_score() * .5);

                }
                else if (grades[i]->get_type() == PROJECT){
                    //the project is worth 35%
                    final_grade += (grades[i]->get_score() * .35);
                }
           }
        }
    }

    grade->setText(QString::number(final_grade, 'g', 4) + "%");
}

