#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    /*~~~~~~~~~~Main Window Set-Up~~~~~~~~~~*/

    ui->setupUi(this);
    centralW = new QWidget();

    this->setWindowTitle("Grade Calculator");

    main_layout = new QGridLayout();


    /*~~~~~~~~~~The 'Top' of the Main Window~~~~~~~~~~*/

    top = new QHBoxLayout;

    head = new QLabel("Course: ");
    head->setAlignment(Qt::AlignRight|Qt::AlignVCenter);
    top->addWidget(head);

    //Dropbox for Course Selection
    course_select = new QComboBox();
    course_select->addItem("PIC10B Intermediate Programming");
    course_select->addItem("PIC10C Advanced Programming");
    QObject::connect(course_select, SIGNAL(textActivated(QString)),
                     this, SLOT(change_class(QString)));
    course_select->setFixedSize(200,25);
    top->addWidget(course_select);

    //Add 'Top' to the Widget
    main_layout->addLayout(top,0,0,1,-1);
    top->setAlignment(Qt::AlignCenter);


    /*~~~~~~~~~~The 'Middle' of the Main Window~~~~~~~~~~*/

    //Create 2 Classes/"Courses"
    pic10b = new Course(PIC10B);
    pic10c = new Course(PIC10C);

    //Each "Course" to the Widget
    main_layout->addLayout(pic10b->get_left(),1,0);
    main_layout->addLayout(pic10b->get_right(),1,1);

    main_layout->addLayout(pic10c->get_left(),1,0);
    main_layout->addLayout(pic10c->get_right(),1,1);

    current_display = PIC10B;
    pic10c->display_off();


    /*~~~~~~~~~~The "Bottom" of the Main Window~~~~~~~~~~*/

    bottom = new QHBoxLayout;

    //Change Grading Schema
    method_A = new QRadioButton("Grading Schema A");
    method_A->click();
    QObject::connect( method_A, SIGNAL( clicked() ),
                     pic10b, SLOT( change_grading() ) );
    QObject::connect( method_A, SIGNAL( clicked() ),
                     pic10c, SLOT( change_grading() ) );
    bottom->addWidget(method_A);

    method_B = new QRadioButton("Grading Schema B");
    QObject::connect( method_B, SIGNAL( clicked() ),
                     pic10b, SLOT( change_grading() ) );
    QObject::connect( method_B, SIGNAL( clicked() ),
                     pic10c, SLOT( change_grading() ) );
    bottom->addWidget(method_B);

    //Calculate Grade Button
    bottom->addWidget(pic10b->get_calculate_button());
    bottom->addWidget(pic10c->get_calculate_button());

    //Display Grade
    btm_label = new QLabel("Overall Grade: ");
    bottom->addWidget(btm_label);
    bottom->addWidget(pic10b->get_grade());
    bottom->addWidget(pic10c->get_grade());

    main_layout->addLayout(bottom,2,0,-1,-1);
    bottom->setAlignment(Qt::AlignCenter);


    /*~~~~~~~~~~Display Main Window~~~~~~~~~~*/

    centralW->setLayout(main_layout);
    setCentralWidget(centralW);

}

MainWindow::~MainWindow()
{
    delete ui;
}

//Accessor of display
course_name MainWindow::get_current_display()
{
    return current_display;
}

//Private Slot: Setter for display
void MainWindow::change_class(QString change_to)
{
    if (change_to == "PIC10B Intermediate Programming"){
        pic10b->display_on();
        pic10c->display_off();
    }else{
        pic10b->display_off();
        pic10c->display_on();
    }
}

