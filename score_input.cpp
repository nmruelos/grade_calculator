#include "score_input.h"

score_input::score_input(QString n, Type x, int s)
    : name(n), type(x), score(s)
{
    //init member widgets
    group = new QWidget();
        /* Note: Adding everything to a widget rather than just
         * a layout allows and easy way to show() and hide()
         * all of the widgets on the Main Window.
         * Not doing so causes the widgets to appear in a
         * separate window when shown again. */

    label = new QLabel(name);

    slider = new QSlider(Qt::Horizontal);
    slider->setRange(0,100);

    spinbox = new QSpinBox();
    spinbox->setRange(0,100);

    //Sets Layout according to Assignment Type
    if (type == HW){
        layout = new QBoxLayout(QBoxLayout::LeftToRight);
        spinbox->setFixedSize(45,20);
    }else{
        layout = new QBoxLayout(QBoxLayout::TopToBottom);
        spinbox->setFixedHeight(20);
    }

    //add widgets to layout
    layout->addWidget(label);
    layout->addWidget(slider);
    layout->addWidget(spinbox);

    //add layout to group
    group->setLayout(layout);

    //connects slider and spinbox
    QObject::connect(slider, SIGNAL(valueChanged(int)),
                     spinbox, SLOT(setValue(int)));
    QObject::connect(spinbox, SIGNAL(valueChanged(int)),
                     slider, SLOT(setValue(int)));

    //connects userinput to int 'score'
    QObject::connect(spinbox, SIGNAL(valueChanged(int)),
                     this, SLOT(change_score(int)));
}

//Accessor for label, slider, and spinbox in vertical layout
QWidget *score_input::get_group() const
{
    return group;
}

//Accessor: ENUM of type of assignment
Type score_input::get_type() const
{
    return type;
}

//Accessor: score
int score_input::get_score() const
{
    return score;
}

//Setter: Turns on the widgets in MainWindow
void score_input::widgets_on()
{
    group->show();
}

//Setter: Turns off widgets in MainWindow
void score_input::widgets_off()
{
    group->hide();
}

//Private Slot: changes value of 'score'
void score_input::change_score(int value)
{
    score = value;
}
