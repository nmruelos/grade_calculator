#ifndef SCORE_INPUT_H
#define SCORE_INPUT_H

#include <QObject>
#include <QWidget>
#include <Qstring>
#include <QLabel>
#include <QSlider>
#include <QSpinBox>
#include <QBoxLayout>

//ENUM for type of assignment
enum Type {
    HW, //Homework (PIC10B) or Assignment (PIC10C)
    MIDTERM,
    FINAL,
    PROJECT //only for PIC10C
};

class score_input : public QObject
{
    Q_OBJECT
public:
    //Constructors
    explicit score_input(QObject *parent = nullptr);
    score_input(QString = "Assignment", Type = HW, int = 0);

    //Virtual Destructor
    //virtual ~score_input();

    //Accessors
    QWidget* get_group() const;
    Type get_type() const;
    int get_score() const;

    //Display or Hide Widgets
    void widgets_on();
    void widgets_off();

private slots:
    void change_score(int);//changes value of score

private:

    QString name;//Name of Assignment
    Type type;//Type of Assignment: see enum above
    int score;//Score out of 100%

    //Widgets To be Displayed
    QLabel * label;
    QSlider * slider;
    QSpinBox * spinbox;

    //Layout of Widgets (Horizonatl or Vertical)
    QBoxLayout * layout;

    QWidget * group;

};

#endif // SCORE_INPUT_H
